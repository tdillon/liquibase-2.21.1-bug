# Liquibase 4.21.1 Bug

There appears to be a bug (or at least a change in behavior) when upgrading from Liquibase version `4.20.0` to `4.21.1`.

This testing pipeline runs the following 2 commands.


```shell
gradle update -PliquibaseVersion=4.20.0
gradle update -PliquibaseVersion=4.21.1
```

Review the logs for both `4.20.0` and `4.21.1`.
Take note of the various paths shown in the logs for both versions and how they differ.

🟩 For `4.20.0` you will see the following which shows `data.csv` is correctly being referenced relative to `changelog.xml`.

```
 Data loaded from 'data/data.csv'
```

🟥 For `4.21.1` you will see the following which shows Liquibase is mishandling the paths when it comes to the `loadData` command.  Specifically, the path to `changelog.xml` and `data.csv` should contain `../../` instead of just `../`.

```
Path ../changelog.xml in /builds/tdillon/liquibase-4.21.1-bug/a/b does not exist
Path ../data/data.csv in /builds/tdillon/liquibase-4.21.1-bug/a/b does not exist
```

My hunch is this issue was introduced by https://github.com/liquibase/liquibase/pull/3853.

# Problem Solved with 4.23.0

https://github.com/liquibase/liquibase/issues/4165